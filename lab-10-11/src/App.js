import './App.css';
import React from 'react';

/*
Богданова Анастасия Александровна М8О-106М-22
1. Выполнить одно из заданий B1-B8, C1-C13 по выбору
С-3 Написать компонент слайдера, который принимает массив ссылок на картинки. Имеет две кнопки назад и вперед.
2. Выполнить 3 задание из Задачника (по одному из разделов 2, 3, 4)
2.1.3 - Дан массив. Удалите из него элементы с заданным значением.
3.1.2 - Дан массив: [1, '', 2, 3, '', 5]. Удалите из массива все пустые строки.
4.1.2 - Сделайте функцию, которая параметром будет получать дату, а возвращать день недели словом, соответствующий этой дате.
*/

const array1 = [1, 2, 3, 4, 5, 3, 6, 3, 4, 5, 6, 7, 3, 2];
const array2 = [1, '', 2, 3, '', 5];

function generateRandomArray(size) {
  var randomArray = [];

  for (var i = 0; i < size; i++) {
    var randomNumber = Math.floor(Math.random() * 100);
    randomArray.push(randomNumber);
  }

  return randomArray;
}


function generateRandomArrayWithGaps(size, numGaps) {
  var randomArray = [];
  for (var i = 0; i < size - numGaps; i++) {
    var randomNumber = Math.floor(Math.random() * 100);
    randomArray.push(randomNumber);
  }
  for (var j = 0; j < numGaps; j++) {
    randomArray.splice(Math.floor(Math.random() * (randomArray.length + 1)), 0, "");
  }

  return randomArray;
}

// 2/1.3
function removeElementsFromArray(arr, value) {
  return arr.filter(item => item !== +value);
}

// 3/1.2
function removeEmptyStringsFromArray(arr) {
  return arr.filter(item => item !== '');
}

// 4/1.2
function getWeekday(dateString) {
  const daysOfWeek = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
  const date = new Date(dateString);
  const dayOfWeekIndex = date.getDay();
  return daysOfWeek[dayOfWeekIndex];
}

const Tasks = () => {
  const [array1Task, setArray1Task] = React.useState(array1)
  const [removeValue, setRemoveValue] = React.useState(3)
  const [array2Task, setArray2Task] = React.useState(array2)
  const [date, setDate] = React.useState(new Date().toDateString())

  const handle1 = () => {
    return setArray1Task(generateRandomArray(10))
  }
  const handle2 = () => {
    return setArray2Task(generateRandomArrayWithGaps(10, 4))
  }

  return <ul className="tasks">
    <li>2.1.3:
      <div className="answer">
        <button className="button" onClick={handle1}>Новый данные</button>
        <div>{JSON.stringify(array1Task)}</div>
        <div><b>Удалить все значения</b></div>
        <input type="number"value={removeValue} onChange={(e) => setRemoveValue(e.target.value)}></input>
        <div>{JSON.stringify(removeElementsFromArray(array1Task, removeValue))}</div>
      </div>
    </li>
    <li>3.1.2:
      <div className="answer">
        <button className="button" onClick={handle2}>Новые данные</button>
        <div>{JSON.stringify(array2Task)}</div>
        <div><b>Убрать пустые строки</b></div>
        <div>{JSON.stringify(removeEmptyStringsFromArray(array2Task))}</div>
      </div>
    </li>
    <li>4.1.2:
      <div className="answer">
        <input type="date" value={date} onChange={e => setDate(e.target.value)}></input>
        <div>{JSON.stringify(date)}</div>
        <div><b>День недели</b></div>
        <div>{getWeekday(date)}</div>
      </div>
    </li>
  </ul>
}


const Slider = ({ images }) => {
  const [currentIndex, setCurrentIndex] = React.useState(0);

  const goToNextSlide = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === images.length - 1 ? 0 : prevIndex + 1
    );
  };

  const goToPrevSlide = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === 0 ? images.length - 1 : prevIndex - 1
    );
  };

  return (
    <div className="slider">
      <button onClick={goToPrevSlide}>Назад</button>
      <img src={images[currentIndex]} alt="Slide" />
      <button onClick={goToNextSlide}>Вперед</button>
    </div>
  );
};


function App() {
  return (
    <div className="App">
      <header className="center">
        <h5 style={{"text-align": "start"}}>
          <p>Богданова Анастасия Александровна М8О-106М-22</p>
          <p>1. Выполнить одно из заданий B1-B8, C1-C13 по выбору</p>
          <p>С-3 Написать компонент слайдера, который принимает массив ссылок на картинки. Имеет две кнопки назад и вперед.</p>
          <p>2. Выполнить 3 задание из Задачника (по одному из разделов 2, 3, 4)</p>
          <p>2.1.3 - Дан массив. Удалите из него элементы с заданным значением.</p>
          <p>3.1.2 - Дан массив: [1, '', 2, 3, '', 5]. Удалите из массива все пустые строки.</p>
          <p>4.1.2 - Сделайте функцию, которая параметром будет получать дату, а возвращать день недели словом, соответствующий этой дате.</p>
        </h5>
      </header>
      <main>
        <section>
          <h2>Лабораторная 10</h2>
          <Slider images={[
            "https://www.kasandbox.org/programming-images/avatars/marcimus-purple.png",
            "https://www.kasandbox.org/programming-images/avatars/marcimus-red.png",
            "https://www.kasandbox.org/programming-images/avatars/marcimus-orange.png",

          ]} />
        </section>
        <section style={{ display: "flex" }}>
          <Tasks />
        </section>
      </main>
    </div>
  );
}

export default App;
