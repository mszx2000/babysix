document
  .getElementById("file-input")
  .addEventListener("change", function (event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
      var jsonData = JSON.parse(e.target.result);
      buildTable(jsonData);
      populateColumnSelect(jsonData);
    };
    reader.readAsText(file);
  });

function buildTable(jsonData) {
  var tableHead = document.getElementById("table-head");
  var tableBody = document.getElementById("table-body");
  tableHead.innerHTML = "";
  tableBody.innerHTML = "";

  var columns = getSelectedColumns();

  var headRow = document.createElement("tr");
  columns.forEach(function (column) {
    var th = document.createElement("th");
    th.textContent = column;
    headRow.appendChild(th);
  });
  tableHead.appendChild(headRow);

  jsonData.forEach(function (rowData) {
    var row = document.createElement("tr");
    columns.forEach(function (column) {
      var cell = document.createElement("td");
      cell.textContent = rowData[column];
      row.appendChild(cell);
    });
    tableBody.appendChild(row);
  });
}

function populateColumnSelect(jsonData) {
  var select = document.getElementById("columns");
  select.innerHTML = "";

  var columns = Object.keys(jsonData[0]);
  columns.forEach(function (column) {
    var option = document.createElement("option");
    option.textContent = column;
    select.appendChild(option);
  });

  for (var i = 0; i < 4 && i < select.options.length; i++) {
    select.options[i].selected = true;
  }

  buildTable(jsonData);

  select.addEventListener("change", function (e) {
    const selected = getSelectedColumns();
    if (
      selected?.length === 0 ||
      selected.length < 4 &&
      selected.includes(select.options[select.selectedIndex ?? 0]?.textContent)
    ) {
      return;
    }
    buildTable(jsonData);
  });
}

function getSelectedColumns() {
  var select = document.getElementById("columns");
  var selectedColumns = [];
  for (var i = 0; i < select.options.length; i++) {
    if (select.options[i].selected) {
      selectedColumns.push(select.options[i].textContent);
    }
  }
  return selectedColumns;
}
